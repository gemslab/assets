
Angry Warlord is a casual, fan game that tells a historical story and challenges you to complete as many levels as you can in a vibrant, unusual world where you are supposed to ride your Rhino to destroy and defeat the enemy.

### GAMEPLAY
It is designed as an easy to play and enjoy the game with amazing magic items to collect during gameplay. These items can increase your performance, speed, and abilities.

ART
Angry Warlord comes with the best visual experience ever. Beautiful, minimalistic and high-end colorful graphics.


### BUILT ON
This game was built on Telos Blockchain

### FEATURES

◆ Abilities panic button
◆ Fast speed with simple controls
◆ Protection shield
◆ Ability to fly
◆ Magic boot
◆ Unique characters
◆ Unique style graphics
◆ Many levels

### Download the game 
https://play.google.com/store/apps/details?id=com.gleamlabs.angrywarlord1&hl=en